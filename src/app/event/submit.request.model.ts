export class SubmitRequest {
    algorithm : string;
    expectedNumber : number;
    heuristic : string;

    constructor(algorithm : string, expectedNumber : number, heuristic : string){
        this.algorithm = algorithm;
        this.expectedNumber = expectedNumber;
        this.heuristic = heuristic;
    }
}
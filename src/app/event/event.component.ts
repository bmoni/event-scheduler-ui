import { Component, OnInit, Input } from '@angular/core';
import { SchedulerServiceService } from '../scheduler-service.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  private algorithms : string[] = ["astar", "backtrack", "bestfirst", "breadthfirstsearch", "depthfirstsearch"];
  private heuristics : string[] = ["average", "locationbase"];

  selectedHeuristic : string;
  selectedAlgorithm : string;
  expectedNumber : number;

  constructor(private service : SchedulerServiceService
            , private _router: Router
            , private _location: Location) { 
    service.router = _router.url;
  }

  ngOnInit() {
    this.getEvents();
  }

  unpin(eventID : string){
    this.service.unpin(eventID,this.getEvents);
  }

  pin(eventID: string){
    this.service.pin(eventID,this.getEvents);
  }

  goBack() {
    this._location.back();
  }

  getEvents = ()=>{
    this.service.getEvents();
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SchedulerServiceService } from '../scheduler-service.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators } from "@angular/forms";

export class durationObject{
  label:string;
  value:number;
  constructor(label:string,value:number){
    this.label = label;
    this.value = value;
  }
}


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  eventName:string;
  location: string;
  startDate:any = null;
  endDate:any = null;
  duration:any = null;
  durationSelections:Array<durationObject> = [];
  locationSelections: Array<string> = [];
  constructor(private http : HttpClient
            , private service : SchedulerServiceService
            , private _router: Router
            , private _location: Location
            , public fb: FormBuilder) { 
    service.router = _router.url;
  }

  getTimeSpan = (minutes:number):string => {
    let h:number = Math.floor(minutes/60);
    let m:number = minutes%60;
    return `${h?h:'00'}:${m?m:'00'}:00`;
 }

   prepareDurationDropdown(){
    let getLabel = (minutes:number):string => {
       let h:number = Math.floor(minutes/60);
       let m:number = minutes%60;
       return `${h?h+'h':''} ${m?m+'mins':''}`;
    }
    for (let i = 1; i <= 40; i++) {
      const asd = new durationObject(getLabel(i*15),i*15)
      this.durationSelections.push(asd);
   }
  }

  durationForm = this.fb.group({
    durationFromControlName: ['']
  })

  locationForm = this.fb.group({
    locationFromControlName: ['']
  })

  get durationFromControlName() {
    return this.durationForm.get('durationName');
  }

  get locationFromControlName() {
    return this.locationForm.get('locationName');
  }

  changeDuration(e){
   this.durationFromControlName.setValue(e.target.value, {
      onlySelf: true
    })
  }

  changeLocation(e){
    this.locationFromControlName.setValue(e.target.value, {
       onlySelf: true
     })
   }

  ngOnInit() {
    this.prepareDurationDropdown();
    this.http.get('http://localhost:50997/api/contact/locations')
      .subscribe((response:Array<string>) => {
        this.locationSelections = response; 
      });
  }

  register(){
    let startDate:Date = this.startDate.toISOString();
    let endDate:Date = this.endDate.toISOString();
    let duration:any = this.durationForm.value.durationFromControlName;
    let location:string = this.locationForm.value.locationFromControlName;
  
   let event =  {
      "name": this.eventName,
      "targetDateIntervall": {
        "start": this.startDate.toISOString(),
        "end": this.endDate.toISOString()
      },
      "duration": this.getTimeSpan(this.durationForm.value.durationFromControlName), 
      "location": this.locationForm.value.locationFromControlName
    }

    let data = {"user-id": this.service.userId,
                "events": []};
    data.events.push(event);

    this.service.registerEvent(data,this.goBack);
  }

  goBack=()=> {
    this._location.back();
  }

}

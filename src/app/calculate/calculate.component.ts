import { Component, OnInit } from '@angular/core';
import { SchedulerServiceService } from '../scheduler-service.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-calculate',
  templateUrl: './calculate.component.html',
  styleUrls: ['./calculate.component.css']
})
export class CalculateComponent implements OnInit {
  expectedNumber:number;
  algorithmSelections: Array<any> = [{'label':'Breadth First Search'
                                     ,'value':'breadthfirstsearch'},
                                     {'label':'Depth First Search'
                                     ,'value':'depthfirstsearch'},
                                     {'label':'Backtrack'
                                     ,'value':'backtrack'},
                                     {'label':'Best First Search'
                                     ,'value':'bestfirst'},
                                     {'label':'A*'
                                     ,'value':'astar'}];
  heuristicSelections: Array<any> = [{'label':'Average'
                                     ,'value':'average'},
                                     {'label':'Location'
                                     ,'value':'locationbase'}];

  constructor(private service : SchedulerServiceService
            , private _router: Router
            , private _location: Location
            , public fb: FormBuilder) {
    service.router = _router.url;
  }

  heuristicForm = this.fb.group({
    heuristicFromControlName: ['']
  })

  algorithmForm = this.fb.group({
    algorithmFromControlName: ['']
  })

  get heuristicFromControlName() {
    return this.heuristicForm.get('heuristicName');
  }

  get algorithmFromControlName() {
    return this.algorithmForm.get('algorithmName');
  }

  changeHeuristic(e){
   this.heuristicFromControlName.setValue(e.target.value, {
      onlySelf: true
    })
  }

  changeAlgorithm(e){
    this.algorithmFromControlName.setValue(e.target.value, {
       onlySelf: true
     })
   }

  ngOnInit() {
  }

  goBack() {
    this._location.back();
  }

  submit(){
    let heur:any = this.heuristicForm.value.heuristicFromControlName;
    let alg:string = this.algorithmForm.value.algorithmFromControlName;
    this.service.submit(alg,this.expectedNumber,heur,null);
  }
}

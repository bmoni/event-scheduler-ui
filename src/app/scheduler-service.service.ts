import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { SubmitRequest } from './event/submit.request.model';

@Injectable({
  providedIn: 'root'
})
export class SchedulerServiceService {
  userId : string;
  events : any;
  result : any;
  router: string;

  constructor(private http : HttpClient, public route : ActivatedRoute) {
    this.userId = this.route.snapshot.queryParams['userId']
   }

   ngOnInit(){
    
   }
   
   getEvents(){
    this.http.get('http://localhost:50997/api/contact/getevents/' + this.userId)
    .subscribe((response) => {this.events = response; console.log(this.userId)});
   }

   submit(algorithm : string, expectedNumber : number, heuristic : string, successCallback:any){
    this.http.post('http://localhost:50997/api/contact/submit/' + this.userId, 
                    new SubmitRequest(algorithm, expectedNumber, heuristic))
             .subscribe((response) => {this.result = response; 
                                       console.log(response);
                                       if(successCallback){successCallback();};
                                });
  }
  acceptResult(result : any,successCallback:any){
    this.http.put(`http://localhost:50997/api/contact/applyEvents/${this.userId}`, result)
             .subscribe((response) => {console.log(response);
                                      if(successCallback){successCallback();}});
  }

  unpin(eventId : string,successCallback:any){
    this.http.put(`http://localhost:50997/api/contact/event/unpin/${eventId}/${this.userId}`, "")
             .subscribe((response) => {console.log(response);
                                       if(successCallback){successCallback();}});
  }

  pin(eventId : string,successCallback:any){
    this.http.put(`http://localhost:50997/api/contact/event/pin/${eventId}/${this.userId}`, "")
             .subscribe((response) => {console.log(response);
                                      if(successCallback){successCallback()}});
  }

  registerEvent(event : any,successCallback:any){
    this.http.post('http://localhost:50997/api/contact/register', event)
             .subscribe((response) => {console.log(response); if(successCallback){successCallback();}});
  }
}

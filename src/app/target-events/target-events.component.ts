import { Component, OnInit } from '@angular/core';
import { SchedulerServiceService } from '../scheduler-service.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-target-events',
  templateUrl: './target-events.component.html',
  styleUrls: ['./target-events.component.css']
})
export class TargetEventsComponent implements OnInit {

  constructor(private service : SchedulerServiceService, private _router: Router, private _location: Location) { 
    service.router = _router.url;
  }

  ngOnInit() {
  }

  acceptResult(result : any){
    let goToRoot = () => {
      this._router.navigate(['/user/events']);
    }
    this.service.acceptResult(result,goToRoot);
    
  }

  goBack() {
    this._location.back();
  }
}

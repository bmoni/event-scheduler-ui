import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetEventsComponent } from './target-events.component';

describe('TargetEventsComponent', () => {
  let component: TargetEventsComponent;
  let fixture: ComponentFixture<TargetEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

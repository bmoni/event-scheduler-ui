import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { EventComponent } from './event/event.component';
import { TargetEventsComponent } from './target-events/target-events.component';
import { UserComponent } from './user/user.component';
import { CalculateComponent } from './calculate/calculate.component';


const routes: Route[] = [
  { 
    path: 'user', 
    component: UserComponent,
    children: [
      { path: 'events', component: EventComponent, children: [
        { path: 'calculate', component: CalculateComponent, children: [
          { path: 'targetevents', component: TargetEventsComponent} ]}] 
      },
      { path: 'register', component: RegisterComponent },
    ]
  },
  { path: '**', redirectTo: '/user'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { SchedulerServiceService } from '../scheduler-service.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  mySubscription: any;
  queryParam;

  constructor(private service : SchedulerServiceService, private _router: Router) {
    service.router = _router.url;

    this._router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    
    this.mySubscription = this._router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this._router.navigated = false;
      }
    });
  }

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.service.route.queryParams.subscribe(params => {
      this.queryParam = params['userId'];
      console.log(this.service.router);
      console.log(this.service.router.includes('/user'));
    });
  }
}

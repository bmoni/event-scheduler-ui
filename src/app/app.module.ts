import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { EventComponent } from './event/event.component';
import { TargetEventsComponent } from './target-events/target-events.component';
import { UserComponent } from './user/user.component';

import { SchedulerServiceService } from './scheduler-service.service';
import { CalculateComponent } from './calculate/calculate.component';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    EventComponent,
    TargetEventsComponent,
    UserComponent,
    CalculateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule, 
    ReactiveFormsModule,
    BrowserAnimationsModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule
  ],
  providers: [SchedulerServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
